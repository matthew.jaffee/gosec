package xyz

import (
	"net"

	"golang.org/x/crypto/ssh"
)

// Foo is a test function
func Foo() {
	_ = ssh.InsecureIgnoreHostKey()

	_, _ = net.Listen(
		"tcp",
		"0.0.0.0:2000",
	)

}
