ARG POST_ANALYZER_SCRIPTS_VERSION=0.0.5
ARG TRACKING_CALCULATOR_VERSION=2.2.4
ARG SCANNER_VERSION=2.9.5

FROM registry.gitlab.com/security-products/post-analyzers/scripts:${POST_ANALYZER_SCRIPTS_VERSION} AS scripts
FROM registry.gitlab.com/security-products/post-analyzers/tracking-calculator:${TRACKING_CALCULATOR_VERSION} AS tracking

FROM golang:1.16-alpine AS build

ARG SCANNER_VERSION
ARG GOSEC_SHA1SUM=933e2a0b6d76a3a4150a33074663e9addab0694c

ENV CGO_ENABLED=0 GOOS=linux

WORKDIR /go/src/buildapp
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

ADD https://github.com/securego/gosec/releases/download/v${SCANNER_VERSION}/gosec_${SCANNER_VERSION}_linux_amd64.tar.gz /tmp/gosec.tar.gz
RUN echo "$GOSEC_SHA1SUM  /tmp/gosec.tar.gz" | sha1sum -c && \
  tar xf /tmp/gosec.tar.gz && \
  rm -f /tmp/gosec.tar.gz && \
  mv gosec /bin/gosec

# Create new base container with a clean $GOPATH
FROM golang:1.16-alpine AS base

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION}

ENV CGO_ENABLED=0 GOOS=linux GO111MODULE=auto

RUN apk --no-cache add git ca-certificates gcc libc-dev && \
    mkdir /.cache && \
    chmod -R g+r /.cache

COPY --from=build /analyzer /analyzer-binary
COPY --from=build /bin/gosec /bin/gosec

COPY --from=tracking /analyzer-tracking /analyzer-tracking
COPY --from=scripts /start.sh /analyzer

ENTRYPOINT []
CMD ["/analyzer", "run"]
