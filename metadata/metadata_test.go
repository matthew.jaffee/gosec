package metadata_test

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/gosec/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

func TestReportScanner(t *testing.T) {
	want := report.ScannerDetails{
		ID:      "gosec",
		Name:    "Gosec",
		Version: metadata.ScannerVersion,
		Vendor: report.Vendor{
			Name: "GitLab",
		},
		URL: "https://github.com/securego/gosec",
	}
	got := metadata.ReportScanner

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
