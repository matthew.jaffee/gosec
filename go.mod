module gitlab.com/gitlab-org/security-products/analyzers/gosec/v2

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/google/go-cmp v0.5.6
	github.com/sirupsen/logrus v1.8.1
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/command v1.5.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.24.1
	gitlab.com/gitlab-org/security-products/analyzers/report/v3 v3.7.0
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.3.0
	gitlab.com/gitlab-org/vulnerability-research/foss/vulninfo/vulninfo-go v1.4.0
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3
	golang.org/x/net v0.0.0-20211209124913-491a49abca63 // indirect
	golang.org/x/sys v0.0.0-20211210111614-af8b64212486 // indirect
	golang.org/x/term v0.0.0-20210615171337-6886f2dfbf5b // indirect
)

go 1.15
